import { Breadcrumb, Row, Col, Badge, InputGroup, Form, Button} from 'react-bootstrap';
import {useParams, useNavigate} from 'react-router-dom';
import {useState, useEffect} from 'react';
import Swal from 'sweetalert2';


export default function ProductDetails() {

const {productId} = useParams();
const Navigate = useNavigate();

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [size, setSize] = useState("");
  const [color, setColor] = useState("");
  const [price, setPrice] = useState("");
  const [endUser, setendUser] = useState("");

 // to activate the product
const archiveProduct = (e) => {
  e.preventDefault();

  return fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        
      })
    })
  .then(res => res.json())
  .then(data => {

    if (data === true) {
      Swal.fire({
          title: "Successful Deactivate",
          icon: "success",
          text: "Crigo"
      })
      Navigate(`/product-details/${productId}`)
    } else {
       Swal.fire({
          title: "Something Wrong",
          icon: "error",
          text: "Please try Again!"
      })
    }
  })
}

useEffect(() => {
     fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
     .then(res => res.json())
     .then(data => {

          setProductName(data.productName)
          setDescription(data.description)
          setSize(data.size)
          setColor(data.color)
          setPrice(data.price)
          setendUser(data.endUser)

     })
    
}, [productId])

  return (

      <Row className="mt-5">
         <Breadcrumb>
          <h2>Deactivate Product</h2>
        </Breadcrumb>
        <hr />
        <Col xs={6}>
          <Form onSubmit={archiveProduct}>
           <fieldset disabled>
             <Form.Group className="mb-3">
               <Form.Label ><b>Product:</b></Form.Label>
                 <Form.Control 
                    type="text" 
                    value = {productName}
                    onChange = {e => setProductName(e.target.value)} 
                    required
                 /> 
             </Form.Group>
              <Form.Group className="mb-3">
               <Form.Label ><b>Description:</b></Form.Label>
                <Form.Control 
                     as="textarea" rows={2}
                     type="text" 
                     value = {description}
                     onChange = {e => setDescription(e.target.value)} 
                     required
                      /> 
             </Form.Group>
              <Form.Group className="mb-3">
               <Form.Label ><b>Size:</b></Form.Label>
               <Form.Control 
                    type="text" 
                    value = {size}
                    onChange = {e => setSize(e.target.value)} 
                    required
                        />
             </Form.Group>
              <Form.Group className="mb-3">
               <Form.Label ><b>Color:</b></Form.Label>
               <Form.Control 
                    type="text" 
                    value = {color}
                    onChange = {e => setColor(e.target.value)} 
                    required
                        />
             </Form.Group>
              <Form.Group className="mb-3">
               <Form.Label ><b>Price:</b></Form.Label>
                <Form.Control 
                    type="number"
                    value = {price}
                    onChange = {e => setPrice(e.target.value)}
                    required
                          />
             </Form.Group>
              <Form.Group className="mb-3">
               <Form.Label ><b>End User:</b></Form.Label>
               <Form.Control 
                     type="text" 
                     value = {endUser}
                     onChange = {e => setendUser(e.target.value)}
                     required
                    />
             </Form.Group>
           </fieldset>
           <Button type="submit">Deactivate</Button>
         </Form>
        </Col>
    </Row>
  );
}