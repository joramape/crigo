import {Accordion, Table, Button, Form, Modal} from 'react-bootstrap';
import {useState, useEffect} from 'react';




export default function ViewUser() {

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [isAdmins, setAdmins] = useState([]);
  const [isClients, setClient] = useState([])


// Display Admin User
const AdminUser = () => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/admin`)
    .then(res => res.json())
    .then((data) => setAdmins(data));
}

// Display Client
const ClientUser = () => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/client`)
    .then(res => res.json())
    .then((data) => setClient(data));
}


  useEffect(() => {

    // Admin properties
    AdminUser();

    // Client Properties
    ClientUser();

  },[])
 
  return (
 
        <Accordion>
          <Accordion.Item eventKey="1">
            <Accordion.Header>View Admin</Accordion.Header>
            <Accordion.Body>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 
                {isAdmins.map((ad) => (
                    <tr>
                    <td key={ad.id}>{ad.firstName}</td>
                    <td key={ad.id}>{ad.lastName}</td>
                    <td key={ad.id}>{ad.email}</td>
                    <td key={ad.id}>
                    <Form.Control 
                    	disabled
	                    type="password" 
	                    value = {"********"}
	                  />
                    </td>
                    <td>
                    <Button variant="info" onClick={handleShow}>Details</Button>
                    </td>
                    </tr>
                  ))}
                <Modal
			        show={show}
			        onHide={handleClose}
			        backdrop="static"
			        keyboard={false}
			      	>
			        <Modal.Header closeButton>
			          <Modal.Title>Modal title</Modal.Title>
			        </Modal.Header>
			        <Modal.Body>
			          I will not close if you click outside me. Don't even try to press
			          escape key.
			        </Modal.Body>
			        <Modal.Footer>
			          <Button variant="secondary" onClick={handleClose}>
			            Close
			          </Button>
			          <Button variant="primary">Understood</Button>
			        </Modal.Footer>
			      </Modal>
               </tbody>
              </Table>
            </Accordion.Body>
          </Accordion.Item>
           <Accordion.Item eventKey="2">
            <Accordion.Header>View Customer user</Accordion.Header>
            <Accordion.Body>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                {isClients.map((client) => (
                    <tr>
                    <td key={client.id}>{client.firstName}</td>
                    <td key={client.id}>{client.lastName}</td>
                    <td key={client.id}>{client.email}</td>
                    <td key={client.id}>
                    <Form.Control 
                    	disabled
	                    type="password" 
	                    value = {"********"}
	                  />
                    </td>
                    <td>
                    <Button variant="info">Details</Button>
                    </td>
                    </tr>
                  ))}
               </tbody>
              </Table>
            </Accordion.Body>
         </Accordion.Item>
     </Accordion>
  );
}
