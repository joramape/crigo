import { Breadcrumb, Button,Container,Row,Col,Card, Form} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2';

export default function CreateProduct() {

  const [productName, setProductName] = useState("");
  const [description, setDescription] = useState("");
  const [size, setSize] = useState("");
  const [color, setColor] = useState("");
  const [price, setPrice] = useState("");
  const [endUser, setendUser] = useState("");

  const [isActive, setIsActive] = useState(false);
  const { user, setUser } = useContext(UserContext);
  const Navigate = useNavigate();

      



  const addProduct = (e) => {
    e.preventDefault();


    fetch(`${process.env.REACT_APP_API_URL}/products/addProducts`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        productName: productName,
        description: description,
        size: size,
        color: color,
        price: price,
        endUser: endUser
      })
    })
    .then(res => res.json())
    .then(data => {

     console.log(data)

      if (data === true) {
        Swal.fire({
          title: "Successful Added Product",
          icon: "success",
          text: "Crigo"
        })
      setProductName("");
      setDescription("");
      setSize("");
      setColor("");
      setPrice("");
      setendUser("");
      setIsActive(false);
      Navigate("/Product");
      } else {
          Swal.fire({
          title: "Something Wrong",
          icon: "error",
          text: "Please try Again!"
        })
      }
    })
  }


useEffect(() => {

	if ((productName !== "" && description !== "" && size !== "" && color !== "" && price !== "" && endUser !== "")) {

			setIsActive(true);
		} else {
			setIsActive(false);
		}

}, [productName, description, size, color, price, endUser]);

  return (
      (user.isAdmin !== true) ? 
      <Navigate to="/" />
       :
     
      <Row className="mt-5">
	      <Breadcrumb>
	        <h2>Add Product</h2>
	      </Breadcrumb>
          <hr />
          <Col md='6'>
          <Form onSubmit={addProduct}>
          <Card className='my-1'>
            <Card.Body className='p-5'>
             <Form.Label>Product: </Form.Label>
                  <Form.Group className="mb-3">
                   <Form.Control 
                          type="text" 
                          value = {productName}
                          onChange = {e => setProductName(e.target.value)} 
                          required
                        /> 
                  </Form.Group>
           
             	  <Form.Group className="mb-3">
             	   <Form.Label>Description: </Form.Label>
                   <Form.Control 
                     as="textarea" rows={2}
                     type="text" 
                     value = {description}
                     onChange = {e => setDescription(e.target.value)} 
                     required
                      /> 
                    </Form.Group>

              <Form.Group className="mb-3">
               <Form.Label>Size: </Form.Label>
                 <Form.Control 
                    type="text" 
                    value = {size}
                    onChange = {e => setSize(e.target.value)} 
                    required
                        />
              </Form.Group>

               <Form.Group className="mb-3">
               <Form.Label>Color: </Form.Label>
                 <Form.Control 
                    type="text" 
                    value = {color}
                    onChange = {e => setColor(e.target.value)} 
                    required
                        />
              </Form.Group>

              <Form.Group className="mb-3">
               <Form.Label>Price: </Form.Label>
                <Form.Control 
                    type="number"
                    value = {price}
                    onChange = {e => setPrice(e.target.value)}
                    required
                          />
              </Form.Group>

              <Form.Group className="mb-3">
               <Form.Label>End User: </Form.Label>
                <Form.Control 
	                type="text" 
	                value = {endUser}
	                onChange = {e => setendUser(e.target.value)}
	                required
                    />
              </Form.Group>
                { isActive ?

       				 <Button variant="primary" type="submit" id="submitBtn" >Submit</Button>
			        :
			        <Button variant="danger" disabled type="submit" id="submitBtn">Submit</Button>

    			}
            </Card.Body>
          </Card>
        </Form>
      </Col>
    </Row>
  );
}
