import { Nav, Breadcrumb, Card, Row, Col, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import {Person, Basket, Cart } from 'react-bootstrap-icons'
import {Navigate, NavLink} from 'react-router-dom'
import UserContext from '../UserContext';




export default function Dashboard() {

const { user } = useContext(UserContext);


 const [isClients, setClient] = useState([]);
 const [isAdmins, setAdmins] = useState([]);
const [products, setProducts] = useState([]);

// Display Product
 const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/all`)
       .then(res => res.json())
       .then((data) => setProducts(data));
       
  }

// Display Client
const ClientUser = () => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/client`)
    .then(res => res.json())
    .then((data) => setClient(data));
}

// Display Admin User
const AdminUser = () => {
  return fetch(`${process.env.REACT_APP_API_URL}/users/admin`)
    .then(res => res.json())
    .then((data) => setAdmins(data));
}



  useEffect(() => {
    // Client Properties
    ClientUser();
    // Admin
    AdminUser();
    // Product
    productData();

  },[])


  return (
    (user.isAdmin !== true) ? 
      <Navigate to="/" />
       :
      <Row className="mt-5">
         <Breadcrumb>
          <h2>Dashboard</h2>
        </Breadcrumb>
        <hr />
        <Col xs={12} md={3}>
            <Card bg="primary" style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Text>
                  <Row>
                  <Col><h1><Person /> Users</h1></Col>
                  <h1>{isClients.length}</h1>
                </Row>
                </Card.Text>
              </Card.Body>
              <Card.Footer><Nav.Link as={NavLink} to="/admin-user">Go to User</Nav.Link></Card.Footer>
            </Card>
        </Col>
        <Col xs={12} md={3}>
            <Card bg="success" style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Text>
                  <Row>
                  <Col><h1><Basket /> Product</h1></Col>
                  <h1>{products.length}</h1>
                </Row>
                </Card.Text>
              </Card.Body>
              <Card.Footer><Nav.Link as={NavLink} to="/product">Go to Product</Nav.Link></Card.Footer>
            </Card>
        </Col>
        <Col xs={12} md={3}>
           <Card bg="warning" style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Text>
                  <Row>
                  <Col><h1><Cart /> Order</h1></Col>
                  <h1>0</h1>
                </Row>
                </Card.Text>
              </Card.Body>
              <Card.Footer><Nav.Link as={NavLink} to="#">Go to Admin</Nav.Link></Card.Footer>
            </Card>
        </Col>
        <Col xs={12} md={3}>
           <Card bg="info" style={{ width: '18rem' }}>
              <Card.Body>
                <Card.Text>
                  <Row>
                  <Col><h1><Person /> Admin</h1></Col>
                  <h1>{isAdmins.length}</h1>
                </Row>
                </Card.Text>
              </Card.Body>
              <Card.Footer><Nav.Link as={NavLink} to="/admin-user">Go to Admin</Nav.Link></Card.Footer>
            </Card>
        </Col>
    </Row>
  );
}