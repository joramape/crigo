import {Button,Row, Breadcrumb, Table}from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link} from 'react-router-dom'
import UserContext from '../UserContext';



export default function Product() {

const { user } = useContext(UserContext);
const [products, setProducts] = useState([]);


 const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		   .then(res => res.json())
		   .then((data) => setProducts(data));
		   
  }

  useEffect(() => {
    productData();
  },[])

  return (
      (user.isAdmin !== true) ? 
      <Navigate to="/" />
       :
  	 <Row className="mt-5">
        <Breadcrumb>
          <h2>Product</h2>
        </Breadcrumb>
       <hr />
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>No.</th>
          <th>Product Name</th>
          <th>Description</th>
          <th>Size</th>
          <th>Color</th>
          <th>Price</th>
          <th>End User</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {products.map((prod, index) => (
          
          <tr>
            <td key={prod.id}>{index}.</td>
            <td key={prod.id}>{prod.productName}</td>
            <td key={prod.id}>{prod.description}</td>
            <td key={prod.id}>{prod.size}</td>
            <td key={prod.id}>{prod.color}</td>
            <td key={prod.id}>{prod.price}</td>
            <td key={prod.id}>{prod.endUser}</td>
            <td >
             <>
            <Button size="sm" variant="primary" type="submit" as={Link} to={`/product-update/${prod._id}`}>Update</Button>
            <Button size="sm" className="my-1" variant="info" type="submit" as={Link} to={`/product-details/${prod._id}`}>Details</Button>
            </>
            </td>
          </tr>
          ))}
     </tbody>
    </Table>
   </Row> 

  );
}