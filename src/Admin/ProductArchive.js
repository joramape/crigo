import {Button,Row, Breadcrumb, Table}from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useParams, Link} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function ProductAchive() {

const {productId} = useParams();
const [products, setProducts] = useState([]);
const [smShow, setSmShow] = useState(false);




// Display all product Archive
 const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/archive`)
		   .then(res => res.json())
		   .then((data) => setProducts(data));
		   
  }

  useEffect(() => {
    productData();
  },[])

  return (
  	 <Row className="mt-5">
        <Breadcrumb>
          <h2>Product Archive</h2>
        </Breadcrumb>
       <hr />
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>Product Name</th>
          <th>Description</th>
          <th>Size</th>
          <th>Color</th>
          <th>Price</th>
          <th>End User</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        {products.map((prod) => (
          
          <tr>
            <td key={prod.id}>{prod.productName}</td>
            <td key={prod.id}>{prod.description}</td>
            <td key={prod.id}>{prod.size}</td>
            <td key={prod.id}>{prod.color}</td>
            <td key={prod.id}>{prod.price}</td>
            <td key={prod.id}>{prod.endUser}</td>
            <td >
             <>
            <Button size="sm" variant="info" type="submit" as={Link} to={`/product-details/${prod._id}`}>Details</Button>
            <Button size="sm" className="my-2" variant="primary" as ={Link} to={`/product-reactivate/${prod._id}`}>Activate</Button>
            </>
            </td>
          </tr>
          ))}
     </tbody>
    </Table>
   </Row> 

  );
}