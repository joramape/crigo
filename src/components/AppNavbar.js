import { Cart, Person } from 'react-bootstrap-icons';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import {  NavLink } from 'react-router-dom';
import '../App.css';


import UserContext from '../UserContext';
import { useContext } from 'react';



export default function AppNavbar() {

  const { user } = useContext(UserContext);
  console.log(user);

  if(user.id === null){

     return (
    <Navbar sticky="top" collapseOnSelect expand="xl" bg="dark" variant="dark" fluid>
      <Container>
        <Navbar.Brand>Crigo</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto ">
                <Nav.Link as={NavLink} to="/">Product</Nav.Link>
                <Nav.Link href="#features">Features</Nav.Link>
                <Nav.Link href="#pricing">Pricing</Nav.Link>
                <NavDropdown title="FAQS" id="collasible-nav-dropdown">
                  <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.2">
                    Another action
                  </NavDropdown.Item>
                  <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item href="#action/3.4">
                    Separated link
                  </NavDropdown.Item>
                  </NavDropdown>
                  </Nav>
                  <Nav>
                  <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                   <Nav.Link eventKey={2} href="#cart">
                     <Cart />
                     </Nav.Link>
               </Nav>     
          </Navbar.Collapse>
      </Container>
    </Navbar>
  );

  }

  if(user.isAdmin === true){
   
     return (
      <>
    <Navbar bg="dark" variant="dark">
    <Navbar.Brand className="mx-5">Admin</Navbar.Brand>
      <Container>
        <Navbar.Toggle />
        <Navbar.Collapse className="justify-content-end">
          <Navbar.Text>
            <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
          </Navbar.Text>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    <div class="sidebar">
        <Nav.Link className="active"><h5><Person /><b>  {user.firstName} {user.lastName}</b></h5></Nav.Link>
        <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
        <Nav.Link as={NavLink} to="/create-product">Create Product</Nav.Link>
        <Nav.Link as={NavLink} to="/product">All Product</Nav.Link>
        <Nav.Link as={NavLink} to="/product-active">Active Product</Nav.Link>
        <Nav.Link as={NavLink} to="/product-archive">Archive Product</Nav.Link>
        <Nav.Link as={NavLink} to="/admin-user">Admin - User</Nav.Link>
    </div>
      </>
      );

  }

  if(user.isAdmin === false){
     return (
    <Navbar sticky="top" collapseOnSelect expand="xl" bg="dark" variant="dark">
      <Container>
        <Navbar.Brand as={NavLink} to="/shop-products/">Crigo</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mx-auto ">
              <Nav.Link as={NavLink} to="/shop-products/">Shop</Nav.Link>
            </Nav>
            <Nav> 
              <Nav.Link as={NavLink} to="/my-account/">My Account</Nav.Link>
                <Nav.Link eventKey={2} href="#cart">
                  <Cart />
                </Nav.Link>
           </Nav>     
          </Navbar.Collapse>
      </Container>
    </Navbar>
  );
  }

 
}