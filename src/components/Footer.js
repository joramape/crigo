import { Container, Col, Row} from 'react-bootstrap';
import React from 'react';



export default function App() {

  return (
    <div>
    <div style={{ backgroundColor: 'rgba(98, 101, 103, 0.5)' }}>
      <Container className="mt-5" >
          <Row>
            <Col className='mb-4 mt-5'>
              <h5 className='text-uppercase'>Contact Us!</h5>

              <ul className='list-unstyled'>
              <li>
              ----------------------------------
              </li>
                <li>
                  <a href='#!'>
                   crigoph@gmail.com
                  </a>
                </li>
              </ul>
            </Col>

            <Col className='mb-4 mt-5'>
              <h5 className='text-uppercase'>About Us!</h5>

              <ul className='list-unstyled'>
              <li>
              ----------------------------------
              </li>
                <li>
                 For Leather crafts and more..... CRIGO is on the Go!
                </li>
              </ul>
            </Col>

            <Col className='mb-4 mt-5'>
              <h5 className='text-uppercase'>Payment</h5>
              <ul className='list-unstyled'>
                <li>
              ----------------------------------
              </li>
                <li>
                 <img src={require('../img/Payment.png')} style={{height : 80, width : 200}}/>
                </li>
              </ul>
            </Col>
          </Row>
      </Container>
      </div>
      <div className='text-center text-white p-5' style={{ backgroundColor: 'rgba(23, 32, 42, 1)' }}>
        © | 2023 | Copyright :
        <a className='text-white mx-4'>
         Crigo Leather
        </a>
      </div>
    </div>
  );
}