import { Breadcrumb, Card, Row, Col, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {useState, useEffect} from 'react';

export default function Highight() {

const [products, setProducts] = useState([]);


 const productData = () => {
    return fetch(`${process.env.REACT_APP_API_URL}/products/active`)
           .then(res => res.json())
           .then((data) => setProducts(data));
           
  }

  useEffect(() => {
    productData();

  },[])


  return (
      <Row>
       <Breadcrumb className='mt-5'>
          <h1 className="mx-auto">FEATURED PRODUCTS</h1>
        </Breadcrumb>
       <hr />
      {products.map((prod) => (
       <Col xs={12} md={3}  className="mt-3">
             <Card style={{ width: '18rem' }}>
              <Card.Img variant="top" src="https://placehold.jp/50x30.png" />
              <Card.Body>
                <Card.Title>{prod.productName}</Card.Title>
                <Card.Text>
                 ₱ {prod.price} 
                  <p className="pt-2">In Stock</p>
                </Card.Text>
                <Button variant="outline-secondary" as={Link} to={`/shop/${prod._id}`}>Select Option</Button>
              </Card.Body>
            </Card>
        </Col>
      ))}
    </Row>
  );
}