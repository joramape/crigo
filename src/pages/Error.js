import {Container, Alert, Button} from 'react-bootstrap';

export default function Error() {
 
  return (
   
    <Container className="my-5">
      <Alert variant="secondary">
        <Alert.Heading>How's it going?!</Alert.Heading>
        <p>
          Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget
          lacinia odio sem nec elit. Cras mattis consectetur purus sit amet
          fermentum.
        </p>
        <hr />
        <div className="d-flex justify-content-end">
          <Button variant="outline-success">
            Close me y'all!
          </Button>
        </div>
      </Alert>
       </Container>
  );
}