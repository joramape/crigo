import {Lock, Bag, ArrowDown, Person, House, Box} from 'react-bootstrap-icons';
import { Alert, Tab, Container, Nav, Card, ListGroup, Col, Row,} from 'react-bootstrap';
import {NavLink} from 'react-router-dom'
import UserContext from '../UserContext'
import {useContext} from 'react'

import Orders from './Orders'



export default function Myaccount() {


const {user} = useContext(UserContext);

  return (

    <Row style={{marginBottom : 200}}>
      <Col xs="12" className="mt-5">
       <h1 className="mb-5">My Account</h1>
       <hr  />
      </Col>
     
<Tab.Container id="left-tabs-example" defaultActiveKey="first">
      <Row>
        <Col sm={3}>
          <Nav variant="pills" className="flex-column"> 
          <Nav.Link eventKey="dashboard"><Box className="mx-3" />Dashboard</Nav.Link>
          <hr />
          <Nav.Link eventKey="orders"><Bag className="mx-3" />Orders</Nav.Link>
          <hr />
          <Nav.Link eventKey="downloads"><ArrowDown className="mx-3" /> Downloads</Nav.Link>
          <hr />
          <Nav.Link eventKey="address"><House className="mx-3" /> Address</Nav.Link>
          <hr />
          <Nav.Link eventKey="account"><Person className="mx-3" /> Accound Details</ Nav.Link>  
          <hr />
          <Nav.Link as={NavLink} to="/logout"><Lock className="mx-3" /> Logout</Nav.Link>
          </Nav>
        </Col>
        <Col sm={9}>
          <Tab.Content className="mx-5">
            <Tab.Pane eventKey="dashboard">
            <span>Hello!  <b>{user.firstName} {user.lastName}</b></span>
            <p className="mt-5">From your account dashboard you can view your recent orders, manage your shipping and billing addresses, and edit your password and account details.</p>
            </Tab.Pane>
            <Tab.Pane eventKey="orders">
             <Orders />
            </Tab.Pane>
             <Tab.Pane eventKey="downloads">

            </Tab.Pane>
             <Tab.Pane eventKey="address">
            
            </Tab.Pane>
             <Tab.Pane eventKey="account">
           
            </Tab.Pane>
              
          </Tab.Content>
        </Col>
      </Row>
    </Tab.Container>

    </Row>
  );
}



