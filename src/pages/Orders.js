import { Alert, Row} from 'react-bootstrap';
import {NavLink} from 'react-router-dom'

import UserContext from '../UserContext';
import {useEffect, useState, useContext} from 'react'


export default function Orders() {

const { user} = useContext(UserContext);
const [order, setOrders] = useState([]);

const Orders = (token) => {
   return fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then((data) => setOrders(data))

}


useEffect(() => { 

  Orders();


    
  }, []);

 
}



