import { Button,Row,Col,Card, Form} from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';




export default function Register() {

 
  const {user} = useContext(UserContext);

  // State hooks to store values of the input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
 
  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

const Navigate = useNavigate();

  // for User Register 
        const registerUser = (e) => {
          e.preventDefault();

          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password : password
            
            })
          })
          .then(res => res.json())
          .then(data => {
            console.log(data);

            if (data === true) {
              Swal.fire({
                title: "Registration successful",
                icon: "success",
                text: "Welcome to Crigo!"
              })
              setFirstName("");
              setLastName("");
              setEmail("");
              setPassword1("");
              setIsActive(false);
              Navigate("/login");
            } else {
              Swal.fire({
                title: "Duplicate email found",
                icon: "error",
                text: "Please provide another email"
              })
            }
          })
        }




  useEffect(() => {

    // Validation to enable submit button when all fields are populated and both passwords match
    if((email !== "" && password !== "" && password2 !== "") && (password === password2) && (password.length && password2.length >= 8)){
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password, password2]);
  
  return (
       
      <Row className="mt-5">
       <Col md='6' className='text-center text-md-start d-flex flex-column justify-content-center'>

          <h1 className="my-5 display-3 fw-bold ls-tight px-3">
            The best offer <br />
            <span className="text-primary">for your business</span>
          </h1>
        </Col>


   
        <Col md='6'>
          <Form onSubmit={registerUser}>
          <Card className='my-5'>
            <Card.Body className='p-5'>
              <Row>
                <Col col='6'>
                  <Form.Group className="mb-3">
                   <Form.Control 
                          type="text" 
                          placeholder="Enter First Name"
                          value = {firstName}
                          onChange = {e => setFirstName(e.target.value)} 
                          required
                        /> 
                  </Form.Group>
                </Col>

                <Col col='6'>
                   <Form.Control 
                          type="text" 
                          placeholder="Enter Last Name"
                          value = {lastName}
                          onChange = {e => setLastName(e.target.value)} 
                          required
                      /> 
                </Col>
              </Row>

              <Form.Group className="mb-3">
                 <Form.Control 
                          type="email" 
                          placeholder="Enter email"
                          value = {email}
                          onChange = {e => setEmail(e.target.value)} 
                          required
                        />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Control 
                            type="password" 
                            placeholder="Password"
                            value = {password}
                            onChange = {e => setPassword1(e.target.value)}
                            required
                          />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Control 
                  type="password" 
                           placeholder="Verify Password" 
                           value = {password2}
                           onChange = {e => setPassword2(e.target.value)}
                           required
                         />
              </Form.Group>

              { isActive ?
                  <Button className='w-100 mb-4' size='md' variant="primary" type="submit" id="submitBtn" >
                  Submit
                  </Button>
                  :
                  <Button className='w-100 mb-4' size='md' variant="danger" disabled type="submit" id="submitBtn">
                  Submit
                  </Button>

                }
            </Card.Body>
          </Card>
        </Form>
      </Col>
    </Row>
  );
}
