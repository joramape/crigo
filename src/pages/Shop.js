import { Cart } from 'react-bootstrap-icons';
import {InputGroup,Form, Col, Row, Card, CardGroup, Button} from 'react-bootstrap';
import {useParams, Link} from 'react-router-dom';
import {useState, useEffect} from 'react';


export default function Shop() {

const {productId} = useParams();

const [productName, setProductName] = useState("");
const [description, setDescription] = useState("");
const [size, setSize] = useState("");
const [color, setColor] = useState("");
const [price, setPrice] = useState("");
const [endUser, setendUser] = useState("");

const  [quantity, setQuantity ] = useState(1); 

const inc = () => {
  setQuantity(quantity + 1);
}

const dec = () => {
  setQuantity(quantity - 1);

  if(quantity === 1){
   setQuantity(quantity + 0);
  }
}


useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
    .then(res => res.json())
    .then(data => {


    setProductName(data.productName)
    setDescription(data.description)
    setSize(data.size)
    setColor(data.color)
    setPrice(data.price)
    setendUser(data.endUser)
    
    })
},[productId])

  return (
    <Row>
    <CardGroup className="mt-5">
      <Col className="mx-5">
         <Card.Img src="https://placehold.jp/80x60.png" />
      </Col>
      <Col>
        <h1>{productName}</h1>
        <h3> ₱ {price}</h3>
        <p className="mt-5"><b>Description : </b> {description}</p>
        <p className="mt-4"><b>Size : </b> {size}</p>
        <p className="mt-4"><b>Color : </b> {color}</p>
        <p className="mt-4"><b>End User : </b> {endUser}</p>
    
        <hr className="mt-5"/>
        <p><b>{productName}</b><span className="mx-5"> ₱ {price}</span></p>
        <hr className="mt-5"/>
        <span>Subtotal : <b><h2>₱ {price * quantity}</h2></b> </span>
         <Form.Group as={Col} md="4" controlId="validationFormikUsername2">
              <Form.Label>Quantity :</Form.Label>
              <InputGroup hasValidation>
              <Form.Control
                type="text"
                placeholder="Username"
                aria-describedby="inputGroupPrepend"
                name="username"
                value={quantity}
                
              />
                <Button onClick={dec} size="sm" className="mx-1" variant="danger">-</Button>
                <Button onClick={inc} size="sm">+</Button>
              </InputGroup>
            </Form.Group>
        <hr/>
        <Button variant="dark"><Cart className="mx-3"  />ADD TO CART</Button>
        <Button variant="dark" className="mx-3" as={Link} to ={`/checkout/${productId}/${quantity}`} >PROCEED TO CHECKOUT</Button>
      </Col>
    </CardGroup>
    </Row>
  );
}
